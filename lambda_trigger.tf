# Api Gateway

resource "aws_lambda_permission" "api_invoke_is_mutant_lambda_permission" {
  statement_id  = "AllowExecutionFromApiGW"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.is_mutant_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.main_rest_api.execution_arn}/*/POST/mutants"
}

resource "aws_lambda_permission" "api_invoke_get_stats_mutants_lambda_permission" {
  statement_id  = "AllowExecutionFromApiGW"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.get_stats_mutants_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.main_rest_api.execution_arn}/*/POST/stats"
}

# Dynamo

resource "aws_lambda_event_source_mapping" "event_source_mapping_dynamo_mutants_records" {
  event_source_arn              = aws_dynamodb_table.mutants_records.stream_arn
  enabled                       = true
  function_name                 = aws_lambda_function.create_stats_mutants_lambda.function_name
  starting_position             = "LATEST"
  batch_size                    = 100
  maximum_record_age_in_seconds = 60
}


