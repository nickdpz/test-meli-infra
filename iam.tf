# CloudWatch

resource "aws_iam_policy" "logging_policy" {
  name        = "${var.stack_id}-logging-policy"
  description = "Log Policy"
  path        = "/"
  policy      = data.aws_iam_policy_document.logging_policy_document.json
}

# Lambdas

# 1

resource "aws_iam_role" "is_mutant_lambda_role" {
  name               = "${var.stack_id}-is-mutant-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy_document.json
}

resource "aws_iam_policy" "is_mutant_lambda_policy" {
  name        = "${var.stack_id}-is-mutant-policy"
  description = "Custom policy lambda is mutants"
  policy      = data.aws_iam_policy_document.is_mutant_lambda_policy_document.json
}

resource "aws_iam_role_policy_attachment" "is_mutant_lambda_policy_attachment" {
  role       = aws_iam_role.is_mutant_lambda_role.name
  policy_arn = aws_iam_policy.is_mutant_lambda_policy.arn
}

resource "aws_iam_role_policy_attachment" "is_mutant_lambda_logs_policy_attachment" {
  role       = aws_iam_role.is_mutant_lambda_role.name
  policy_arn = aws_iam_policy.logging_policy.arn
}


# 2

resource "aws_iam_role" "get_stats_mutants_lambda_role" {
  name               = "${var.stack_id}-get-stats-mutants-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy_document.json
}

resource "aws_iam_policy" "get_stats_mutants_lambda_policy" {
  name        = "${var.stack_id}-get-stats-mutants-policy"
  description = "Custom policy lambda get stats mutants"
  policy      = data.aws_iam_policy_document.get_stats_mutants_lambda_policy_document.json
}

resource "aws_iam_role_policy_attachment" "get_stats_mutants_lambda_policy_attachment" {
  role       = aws_iam_role.get_stats_mutants_lambda_role.name
  policy_arn = aws_iam_policy.get_stats_mutants_lambda_policy.arn
}

resource "aws_iam_role_policy_attachment" "get_stats_mutants_lambda_logs_policy_attachment" {
  role       = aws_iam_role.get_stats_mutants_lambda_role.name
  policy_arn = aws_iam_policy.logging_policy.arn
}

# 3

resource "aws_iam_role" "create_stats_mutants_lambda_role" {
  name               = "${var.stack_id}-create-stats-mutants-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy_document.json
}

resource "aws_iam_policy" "create_stats_mutants_lambda_policy" {
  name        = "${var.stack_id}-create-stats-mutants-policy"
  description = "Custom policy lambda create stats mutants"
  policy      = data.aws_iam_policy_document.create_stats_mutants_lambda_policy_document.json
}

resource "aws_iam_role_policy_attachment" "create_stats_mutants_lambda_policy_attachment" {
  role       = aws_iam_role.create_stats_mutants_lambda_role.name
  policy_arn = aws_iam_policy.create_stats_mutants_lambda_policy.arn
}

resource "aws_iam_role_policy_attachment" "create_stats_mutants_lambda_logs_policy_attachment" {
  role       = aws_iam_role.create_stats_mutants_lambda_role.name
  policy_arn = aws_iam_policy.logging_policy.arn
}
