# Default
data "aws_iam_policy_document" "lambda_assume_role_policy_document" {
  statement {
    sid     = "LambdaRoleAssume"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

# Custom 
data "aws_iam_policy_document" "logging_policy_document" {
  statement {
    sid    = "LogginCW"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }
}

# Lambdas

# 1
data "aws_iam_policy_document" "is_mutant_lambda_policy_document" {
  statement {
    sid     = "VerifyMutantAndSave"
    effect  = "Allow"
    actions = ["dynamodb:PutItem", "dynamodb:UpdateItem"]
    resources = [
      aws_dynamodb_table.mutants_records.arn,
      "${aws_dynamodb_table.mutants_records.arn}/*"
    ]
  }
}

# 2
data "aws_iam_policy_document" "get_stats_mutants_lambda_policy_document" {
  statement {
    sid     = "QueryDynamoStats"
    effect  = "Allow"
    actions = ["dynamodb:GetRecords", "dynamodb:Query"]
    resources = [
      aws_dynamodb_table.mutants_stats.arn,
      "${aws_dynamodb_table.mutants_stats.arn}/*"
    ]
  }
}

# 3
data "aws_iam_policy_document" "create_stats_mutants_lambda_policy_document" {
  statement {
    sid     = "RecordDynamoStats"
    effect  = "Allow"
    actions = ["dynamodb:PutItem", "dynamodb:UpdateItem"]
    resources = [
      aws_dynamodb_table.mutants_stats.arn,
      "${aws_dynamodb_table.mutants_stats.arn}/*"
    ]
  }
  statement {
    sid     = "QueryDynamoStats"
    effect  = "Allow"
    actions = ["dynamodb:GetRecords", "dynamodb:GetShardIterator", "dynamodb:ListShards", "dynamodb:DescribeStream", "dynamodb:ListStream"]
    resources = [
      aws_dynamodb_table.mutants_records.arn,
      "${aws_dynamodb_table.mutants_records.arn}/*"
    ]
  }
}
