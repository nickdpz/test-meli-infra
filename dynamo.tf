resource "aws_dynamodb_table" "mutants_records" {
  name         = "${var.stack_id}-mutants-records-table"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "DATA_TYPE"
  range_key    = "DATE"
  tags         = local.common_tags

  attribute {
    name = "DATA_TYPE"
    type = "S"
  }

  attribute {
    name = "DATE"
    type = "S"
  }

  local_secondary_index {
    name            = "date-index"
    range_key       = "DATE"
    projection_type = "ALL"
  }

  stream_enabled   = true
  stream_view_type = "NEW_IMAGE"

}

resource "aws_dynamodb_table" "mutants_stats" {
  name         = "${var.stack_id}-mutants-stats-table"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "DATA_TYPE"
  range_key    = "DATE"
  tags         = local.common_tags

  attribute {
    name = "DATA_TYPE"
    type = "S"
  }

  attribute {
    name = "DATE"
    type = "S"
  }

  attribute {
    name = "COUNT_MUTANT_DNA"
    type = "N"
  }

  attribute {
    name = "COUNT_HUMAN_DNA"
    type = "N"
  }

  local_secondary_index {
    name            = "date-index"
    range_key       = "DATE"
    projection_type = "ALL"
  }

  local_secondary_index {
    name            = "mutant-dna-index"
    range_key       = "COUNT_MUTANT_DNA"
    projection_type = "ALL"
  }

  local_secondary_index {
    name            = "human-dna-index"
    range_key       = "COUNT_HUMAN_DNA"
    projection_type = "ALL"
  }

  stream_enabled = false

}
