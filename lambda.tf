resource "aws_lambda_function" "is_mutant_lambda" {
  function_name    = "${var.stack_id}-is-mutant"
  role             = aws_iam_role.is_mutant_lambda_role.arn
  handler          = local.lambda_node_handler
  runtime          = local.lambda_node_runtime
  source_code_hash = local.lamba_body
  filename         = local.lambda_filename
  tags             = local.common_tags
  environment {
    variables = {
      "NODE_ENV" = "${var.environment}"
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.is_mutant_lambda_policy_attachment,
    aws_cloudwatch_log_group.is_mutant_lambda_log_group,
  ]

  lifecycle {
    ignore_changes = [
      filename,
      source_code_hash
    ]
  }
}

resource "aws_lambda_function" "get_stats_mutants_lambda" {
  function_name    = "${var.stack_id}-get-stats-mutants"
  role             = aws_iam_role.get_stats_mutants_lambda_role.arn
  handler          = local.lambda_node_handler
  runtime          = local.lambda_node_runtime
  source_code_hash = local.lamba_body
  filename         = local.lambda_filename
  tags             = local.common_tags
  environment {
    variables = {
      "NODE_ENV" = "${var.environment}"
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.get_stats_mutants_lambda_policy_attachment,
    aws_cloudwatch_log_group.get_stats_mutants_lambda_log_group,
  ]

  lifecycle {
    ignore_changes = [
      filename,
      source_code_hash
    ]
  }
}


resource "aws_lambda_function" "create_stats_mutants_lambda" {
  function_name    = "${var.stack_id}-create-stats-mutants"
  role             = aws_iam_role.create_stats_mutants_lambda_role.arn
  handler          = local.lambda_node_handler
  runtime          = local.lambda_node_runtime
  source_code_hash = local.lamba_body
  filename         = local.lambda_filename
  tags             = local.common_tags
  environment {
    variables = {
      "NODE_ENV" = "${var.environment}"
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.create_stats_mutants_lambda_policy_attachment,
    aws_cloudwatch_log_group.create_stats_mutants_lambda_log_group,
  ]

  lifecycle {
    ignore_changes = [
      filename,
      source_code_hash
    ]
  }
}
