resource "aws_cloudwatch_log_group" "is_mutant_lambda_log_group" {
  name              = "/aws/lambda/${var.stack_id}-is-mutant"
  retention_in_days = 1
}

resource "aws_cloudwatch_log_group" "get_stats_mutants_lambda_log_group" {
  name              = "/aws/lambda/${var.stack_id}-get-stats-mutants"
  retention_in_days = 1
}

resource "aws_cloudwatch_log_group" "create_stats_mutants_lambda_log_group" {
  name              = "/aws/lambda/${var.stack_id}-create-stats-mutants"
  retention_in_days = 1
}
