terraform {
  backend "s3" {
    region  = "us-east-2"
    key     = "meli/serverless_env.tfstate"
    encrypt = false
    bucket  = "infra-state-test"
  }
  #required_version = "~> 1.0.11"
}
