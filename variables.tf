variable "stack_id" {
  type        = string
  description = "Stack for project resources"
}

variable "environment" {
  type        = string
  description = "Environment owner accout aws"
}
