# Test-Meli-Infra

Infraestructura reto mutantes de mercado libre 

## Manage repository 🤳

- Configure aws
```sh
$ export AWS_ACCESS_KEY_ID=""
$ export AWS_SECRET_ACCESS_KEY=""
```

- Lint code
```
$ terraform fmt
```

## Manage production environment 🎮

- Set environment pro
```
$ sed -i "s/env/pro/g" terraform.tf 
```

- Validate
```sh
$ terraform validate
```

- Plan
```sh
$ terraform plan -var-file ./config/pro.tfvars
```

- Apply
```sh
$ terraform apply -var-file ./config/pro.tfvars
```

- Apply
```sh
$ terraform apply -var-file ./config/pro.tfvars -auto-approve
```